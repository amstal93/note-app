require('dotenv').config({path:'../env/backend.env'});
const {Sequelize} = require('sequelize');
const force=true;
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD,{
    host: process.env.HOST,
    dialect: process.env.DB_DIALECT,
    port: process.env.DB_PORT,
    pool:{
        min: 0,
        max: 10,
        idle: 3000,
        acquire: 5000
    },
    timestamps:true,
    logging: false
});
const sq={};
sq.sequelize=sequelize;
sq.Sequelize=Sequelize;
sq.Note=require('./models/note.model')(sequelize, Sequelize);
module.exports=sq;