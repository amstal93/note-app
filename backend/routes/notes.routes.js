const router = require('express').Router();
const controllers = require('../controllers/notes.controllers');
router.post('/notes', controllers.notes_post);
router.get('/notes', controllers.notes_get);
router.patch('/notes',controllers.update_note);
module.exports=router;