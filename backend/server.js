const app=require('./app');
require('dotenv').config({path:'../env/backend.env'});
const port = process.env.SERVER_PORT;

app.listen(port, ()=>{
    console.log(`Server running at port ${port}.Press Crl+C to stop.`);
});