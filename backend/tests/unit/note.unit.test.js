const nodeMocks=require('node-mocks-http');
const noteMock=require('../mockData/note-mock.json');
const resolvedNote = require('../mockData/note-resolved.json');
const rejectedNote = require('../mockData/note-rejected.json');
const notesMock=require('../mockData/notes-mock.json');
const db = require('../../db.config');
const Note=db.Note;
const Notecontrollers = require('../../controllers/notes.controllers');
let req, res;
beforeEach(()=>{
    req=nodeMocks.createRequest();
    res=nodeMocks.createResponse();
});

describe('notes_post test suite',()=>{
    test('notes_post must be a function',()=>{
        expect(typeof Notecontrollers.notes_post).toBe('function');
    })
    test('Note.create must be called',()=>{
        Note.create=jest.fn();
        let resolvedPromise = Promise.resolve(resolvedNote);
        Note.create.mockReturnValue(resolvedPromise);  
        Notecontrollers.notes_post(req,res);
        expect(Note.create).toBeCalled();
    });
    test('Note.create must be called with req.body',()=>{
        Note.create=jest.fn();
        req.body= noteMock;
        let resolvedPromise = Promise.resolve(resolvedNote);
        Note.create.mockReturnValue(resolvedPromise);
        Notecontrollers.notes_post(req,res);
        expect(Note.create).toBeCalledWith(req.body);
    });
    test('Should return a 201 response code',async ()=>{
        Note.create=jest.fn();
        let resolvedPromise = Promise.resolve(resolvedNote);
        Note.create.mockReturnValue(resolvedPromise);
        await Notecontrollers.notes_post(req,res);
        expect(res.statusCode).toBe(201);
    });
});

describe('notes_get test suite', ()=>{
    test('notes_get must be a function',()=>{
        expect(typeof Notecontrollers.notes_get).toBe('function');
    });
    test('Note.findAll must be called', async ()=>{
        Note.findAll=jest.fn();
        let resolvedPromise = Promise.resolve(resolvedNote);
        Note.findAll.mockReturnValue(resolvedPromise);
        await Notecontrollers.notes_get(req,res);
        expect(Note.findAll).toBeCalled();
    });
    test('Must return 200 status code', async()=>{
        Note.findAll=jest.fn();
        let resolvedPromise = Promise.resolve(notesMock);
        Note.findAll.mockReturnValue(resolvedPromise);
        await Notecontrollers.notes_get(req, res);
        expect(res.statusCode).toBe(200);
    });
});

describe('update_note test suite',()=>{
    test('update_note must be a function',()=>{
        expect(typeof Notecontrollers.update_note).toBe('function');
    });
    test('Note.findByPk must be called', async ()=>{
        Note.findByPk=jest.fn();
        let resolvedPromise = Promise.resolve(noteMock);
        Note.findByPk.mockReturnValue(resolvedPromise);
        await Notecontrollers.update_note(req,res);
        expect(Note.findByPk).toBeCalled();
    });

    test('Note.findByPk ust be called with req.query.id',async ()=>{
        Note.findByPk = jest.fn();
        let resolvedPromise = Promise.resolve(noteMock);
        Note.findByPk.mockReturnValue(resolvedPromise);
        await Notecontrollers.update_note(req,res);
        expect(Note.findByPk).toBeCalledWith(req.query.id); 
    });
    test('Must return 200 status code when correct',async()=>{
        let resolvedPromise=Promise.resolve(noteMock);
        Note.findByPk.mockResolvedValue(resolvedPromise);
        await Notecontrollers.update_note(req, res);
        expect(res.statusCode).toBe(200);
    });
});

describe('', async ()=>{
    
});