exports.handleErrors=function (err){
    let errors={text:'', msg:''}
    if (err.name.includes('SequelizeValidationError')){
        Object.values(err.errors).forEach(({message, path})=>{
            errors[path]=message;
        })
    }
    if(err.name.includes('SequelizeDatabaseError')){
        errors.msg='An error has ocurred'
    }
    if(err.message==='content is null'){
        errors.text='Content can not be empty';
    }
    if(err.message==='content is an empty string'){
        errors.text='Content can not be an empty string';
    }
    return errors;
}